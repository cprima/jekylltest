---
title: index
layout: index
---





<style>
.outer {
  display: table;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
}

.middle {
  display: table-cell;
  vertical-align: middle;
}

.inner {
  margin-left: auto;
  margin-right: auto;
  width: 400px;
  /* Whatever width you want */
}

.junction {
    text-align: center;
}


a:link {
    color: #cb4b16;
    text-decoration: none;
}
a:visited {
    color: #cb4b16;
    text-decoration: none;
}
a:hover {
    color: #cb4b16;
    text-decoration: none;
}
a:active {
    color: #cb4b16;
    text-decoration: none;
}

</style>


<div class="outer">
  <div class="middle">
    <div class="inner">
        <div class="container">
        <div class="row">
            <div class="junction col-sm">
            <h1 class="junction">the last week dot in</h1>
            </div>
        </div>
        <div class="row">
            <div class="junction col-sm">
            <a class="junction" href="/rpa">/RPA</a>
            </div>
            <div class="junction" class="col-sm">
            <a class="junction" href="/lowcode">/LowCode</a>
            </div>
        </div>
        </div>
    </div>
  </div>
</div>