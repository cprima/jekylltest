---
permalink: /blog/UiPath-releases
categories: ["alm"]
title: UiPath Releases, Release Notes and Enterprise Adoption
layout: default
date: 2022-11-13
author: Christian Prior-Mamulyan
status: draft
---

<h2>{{ page.title }}</h2>
<p>Published: {{ page.date | date: "%F" }}<br />Author: {{ page.author }}<br />Status: {{ page.status }}</p>

<style>
dl.csv { list-style: none; margin: 0; padding: 0; margin-bottom: 10px;}
dl.csv dd { display: inline; }
dl.csv dd:after { content: ","; }
dl.csv dd:last-child:after { content: ""; }

blockquote {
  background: #eee8d5;
  border-left: 10px solid #859900;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}
blockquote:before {
  color: #ccc;
  content: open-quote;
  font-size: 4em;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}
blockquote p {
  display: inline;
}
.info-tip {
  background: #eee8d5;
  border-left: 10px solid #859900;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}
.info-tip:before {
  color: #859900;
  content: "🛈";
  margin-right: 0.25em;
}
.info-tip {
  display: block;
}

</style>

UiPath maintains for each product several versions according to their published support status. Viewed over a timeline of the past three years almost every month brought releases, and most months will have product releases for the current versions as well as fixes for older, still supported versions. Conceptually the following chart depicts big months and lesser: The big bubbles contain at least one release with currently mainstream support; middle-sized bubbles contain releases with up to extended-support status. The chart is generated at the specific date as shown in it's title.

But that graph is only the tip of the iceberg of what any RPA Process Owner should understand -- about both UiPath releases as well as one's own installed base.

<figure>
 <embed type="image/svg+xml" src="/tec/phy/UiPath/images/rpa-alm-uipath-releases-timeline_v2022-10-31.svg" />
 <figcaption style="text-align: center;">&copy;2022 by Christian Prior-Mamulyan &middot; licensed under CC BY-ND 4.0 &middot; <a href="/tec/phy/UiPath/images/rpa-alm-uipath-releases-timeline_v2022-10-31.svg" target="tec">open in new tab</a></figcaption>
</figure>


<hr />

<!--<h3>How to stay ~~up-to-date with~~ informed about UiPath releases</h3>-->
<h3>Staying up-to-date with UiPath product release notes</h3>

In an enterprise setting the fast pace of the vendor's product development and a customer's individual IT Risk Management with a typically trailing update cycle poses a challenge: The informed community participant may incorporate new features only with a time lag into corporate projects. These projects will typically differ in their installed versions, and each process may even have a differing dependency graph. At a minimum an RPA process owner should have a good understanding on the past releases for both UiPath products as well as activity dependencies. This page describes how to methodogically gain such an understanding.

<h4>Read the fine manuals</h4>

UiPath has an extensive product familiy and the best starting point of a journey into their releases is <a href="https://docs.uipath.com/overview-guide/docs/product-lifecycle" target="uipath">the product lifecycle page</a>. This page holds within each Automation Pillar for each product the support status, for example for these core products:

<dl class="csv">
<dt>Build</dt>
<dd>Studio</dd>
<dd>Studio X</dd>
</dl>

<dl class="csv">
<dt>Run</dt>
<dd>Robot</dd>
</dl>

<dl class="csv">
<dt>Manage</dt>
<dd>Orchestrator</dd>
</dl>

For each releases version the Support Model, the initial Release Date, the end of the Mainstream Support and the end of the Extended Support is published.

The end of the Mainstream Support is typically the cornerstone for informed decisions in an enterprise setting. At least every quarter a RPA Process Owner should compare his software inventory against this publication. At minimum he should passively participate in the update strategy of the respective Center of Excellence to keep up to date with Platform updates.

Once a product is out of support the information os moved to <a href="https://docs.uipath.com/overview-guide/docs/out-of-support-versions" target="uipath">the page with out of support versions</a>.


Equipped with specific version numbers from both UiPath products and process dependencies the next stop is over at 
<a href="https://docs.uipath.com/releasenotes" target="uipath">the release notes pages</a>. Clicking through to a product's documentation page will prominently feature the release notes on the top of the left sidebar menu. The breadcrumb navigation gives access to earlier major versions and this is where the typical enterprise installed base will be located.

Tip: As an Process Owner of RPA processes the release notes shoud be studied the latest when the next maintenance updates are announced by the enterprise's Center of Excellence.


<h4>Compatibility</h4>

To actively work with the information presented and to map these with one's own software inventory, a thorough understand of the (backwards) compatibility is needed. The official statements by UiPath are found at <a href="https://docs.uipath.com/overview-guide/docs/compatibility-matrix" target="uipath">the compatibility matrix page</a>. Here are major excerpts:

> Studio and Robot always need to have the same version when installed on the same machine.<br />
> UiPath guarantees backwards compatibility between two consecutive released versions of the UiPath on-premises products (standalone and Automation Suite).<br />
> We support backward compatibility, except for breaking changes announced in the official release notes, and following the product lifecycle.<br />
> Newer Robots can execute projects created with older versions of Studio.<br />
> We do not support forward compatibility. Projects created with newer versions of Studio might not work with older Robots.<br />
> Activity package versions are independent of Studio, Robot, or Orchestrator versions, except for Orchestrator activities.

The last quote deserves special attention, and the distinction between product and activity releases is reflected in different resource locations.


<h4>Products and Activities</h4>

The activities have a lifecycles of their own. The vendor's  documentation at <a href="https://docs.uipath.com/activities" target="uipath">the UiPath Activities Guide</a> references each 🧱 UiPath activity in the left sidebar menu, typically starting with a link to the release notes.

Different from the products all release notes are published on a single page, with a table of contents on the right side.

A RPA Process Owner should actively monitor activities that are widely used in the own projects, at minimum for breaking changes. Breaking changes are listed in the right menu with a prominent sub chapter. Here is an example related to UiAutomation Classic version v21.10.3 <a href="https://docs.uipath.com/activities/docs/release-notes-uipath-uiautomation-activities#breaking-changes-1" target="uipath">https://docs.uipath.com/activities/docs/release-notes-uipath-uiautomation-activities#breaking-changes-1</a>

> In certain conditions, such as using a maximized Chrome window, the Click activity with the Click offset Anchoring Point property set to BottomRight triggered an extra scroll event. This behaviour is now fixed, and may cause breaking changes in older workflows.

A notable release path is that of the activity UiPath Automation Modern / UiAutomationNext which got merged into a unified "UiAutomation" activity package. The compatible versions are documented on <a href="https://docs.uipath.com/activities/docs/about-the-ui-automation-next-activities-pack" target="uipath">the page 'About the UIAutomation Modern Activities Package'</a>. Long story short: At the time of writing (October 2022) each RPA Process Owner should be in full swing change-requesting updates to his codebase to leverage the substantial improvements on not just code maintainability.

Managing dependecies should be done from within an individual UiPath Studi oproject, as the release notes do not contain information about compatibility with UiPath Studio versions. Rather this information is found in an activitiy package's description text, visible in the Manage Package feature of the UiPath Studio. It is also possible to set the dependency rule as strict or lowest applicable version for some finer control.

The release notes of each UiPath Studio version contain at the bottom of the page the activity packages and their versions that came included with the installer. These versions serve as a baseline for dependencies in an individual project. Most likely these decisions are only made when maintaining project templates as a CoE.

<h4>Adopting to an enterprise setting</h4>

To put this into practice the broader situation at the clients of UiPath needs some consideration.

Out of a risk mitigation strategy any enterprise setting will typically be trailing a vendor's release schedule by some margin. Whereas a test or integration environment might be following closer the tip of the release branches any production environment typically sees the previous LTS version installed.

<img src="/biz/IT/images/IT-Risk-Management_release-schedules.png" width="100%" height="auto" style="border-top: 1px solid #839496; border-bottom: 1px solid #839496;" />

For the practitioner following vendor announcements or participating in the community this means an upcomoing maintenance upgrade will bring features that are not currently a hotly communicated topic. Hence the list of resources in the previous paragraphs: To enable quick access to the relevant publications.

At the time of writing on 2022-10-09 these were the released versions:

<dl class="csv">
<dt>Orchestrator</dt>
<dd>v2022.10: beta</dd>
<dd>v2022.4: LTS</dd>
<dd>v2021.10: <strong>LTS-1</strong></dd>
<dd>v2020.10: <strong>LTS-2</strong></dd>
</dl>

<dl class="csv">
<dt>Studio</dt>
<dd>v2022.10: beta</dd>
<dd>v2022.4: LTS</dd>
<dd>v2021.10: <strong>LTS-1</strong></dd>
<dd>v2020.10: <strong>LTS-2</strong></dd>
</dl>

<dl class="csv">
<dt>Robot</dt>
<dd>v2022.10: beta</dd>
<dd>v2022.4: LTS</dd>
<dd>v2021.10: <strong>LTS-1</strong></dd>
<dd>v2020.10: <strong>LTS-2</strong></dd>
</dl>

With the assumption that updates were performed not more than 3 months ago this would put the production Orchestrator at LTS-2 version 2020.10.17 relased on 2022-08-03 and the test Orchestrator at LTS-1 version 2021.10.7 released 2022-08-17.

<div class="info-tip">
Methodogically reading up on the release notes since notes would bring the following reading list (because these releases were made between 2022-08-03 and the time of writing 2022-10-09):

<dl class="csv">
<dt>Production</dt>
<dd>Orchestrator: -</dd>
<dd>Studio: <a href="https://docs.uipath.com/studio/v2020.10/docs/release-notes-2020-10-15" target="uipath">2020.10.15</a></dd>
<dd>Robot: <a href="https://docs.uipath.com/robot/v2020.10/docs/robot-assistant-2020-10-15" target="uipath">2020.10.15</a></dd>
</dl>


<dl class="csv">
<dt>Test</dt>
<dd>Orchestrator: <a href="https://docs.uipath.com/orchestrator/v2021.10/docs/release-notes-2021-10-7" target="uipath">2021.10.7</a>, <a href="https://docs.uipath.com/orchestrator/v2021.10/docs/release-notes-2021-10-8" target="uipath">2021.10.8</a></dd>
<dd>Studio: <a href="https://docs.uipath.com/studio/v2021.10/docs/release-notes-2021-10-9" target="uipath">2021.10.9</a></dd>
<dd>Robot: <a href="https://docs.uipath.com/robot/v2021.10/docs/release-notes-2021-10-9" target="uipath">2022-10-05</a></dd>
</dl>

</div>

In case the Robot versions are trailing further behind the Orchestrator (which is fine as per above linked backward compatibility) the reading list might become a bit longer. But this whole research should be done at least on Orchestrator updates anyways.

It is the specific knowledge of the documented changes, bug fixes and breaking changes relevant to the setup that will enable a RPA Process Owner to make informed decisions and steer development and support.

<hr />

<h3>Making Of</h3>

In case that the interactive chart on the top of this page is of interest: Different from the usual visualization of Git branch history there is no common root element. This requirement is fulfilled by the chart type "Dot" of the Python-based library <a href="https://www.pygal.org/" target="tec">PyGal</a>.

The significant data underneath the chart is kept in a PostgreSQL table, chosen for the Pivot table capabilities of the <a href="https://www.postgresql.org/docs/current/tablefunc.html" target="tec">crosstab in the tablefun module</a>. A query then writes out a csv file for convenient consumption by PyGal.

A Jupyter Notebook holds the Python script to conditionally set the configuration for the chart elements and writes out to a SVG file.

Final touches are then made in <a href="https://inkscape.org/" target="tec">Inkscape</a>, like the branching line of the UiAutomationNext package.

To open the SVG file in an own browser tab <a href="/tec/phy/UiPath/images/rpa-alm-uipath-releases-timeline_v2022-10-08.svg" target="tec">this link should do the trick</a>.

Sorry for the not fully intuitive popup behavior, it is the out-of-the-box functionality of of PyGal and not easy to modify. Better to have additional info available at all, albeit with some usability issues.
