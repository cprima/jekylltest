SHELL := /bin/bash

copy:
	scp -r ../media/tlwin/rpa/isoweek/*csv dat/rpa/isoweek/
	scp -r ../media/tlwin/lowcode/isoweek/*csv dat/lowcode/isoweek/
	scp -r ../media/tlwin/rpa/tagged/*csv dat/rpa/tagged/
	scp -r ../media/tlwin/rpa/tagged/*md app/rpa/tagged/
	scp -r ../media/tlwin/rpa/isoweek/*md app/rpa/_posts/
	scp -r ../media/tlwin/lowcode/tagged/*csv dat/lowcode/tagged/
	scp -r ../media/tlwin/lowcode/tagged/*md app/lowcode/tagged/
	scp -r ../media/tlwin/*csv dat/

serve: copy
	bundle exec jekyll serve --host 0.0.0.0 --livereload  --future

commit:
	git add dat app
	git commit -m "Updating data"
	git push gitlabpub
	git push www

publish: copy commit
