---
title: thelastweek.in/LowCode
permalink: /lowcode/
---



<aside>
    <h3>Last week in LowCode</h3>
<ul>
{% for post in site.posts limit:5 %}
{% if post.realm == 'lowcode' %}
<li><a class="post-link" href="{{ post.url | prepend: site.baseurl }}">
{{ post.date | date: "%Y week %V" }}
</a>
</li>
{% endif %}
{% endfor %}
</ul>
</aside>


<!-- <div style="text-align: center; padding-top: 0px; padding-bottom: 30px;"><img style="width: 90%; height: auto; max-width: 640px;" src="/biz/marketing/underconstruction.png" /></div>-->

<style>
dl.csv { list-style: none; margin: 0; padding: 0; margin-bottom: 10px;}
dl.csv dd { display: inline; }
dl.csv dd:after { content: ","; }
dl.csv dd:last-child:after { content: ""; }
</style>

 <div class="row">
  <div class="col-6">
  <dl class="csv">
  <dt>Technology</dt>
  <dd>RPA</dd>
  <dd>LowCode</dd>
  <dd>Machine Learning</dd>
  <dd>OCR</dd>
  </dl>

  <dl class="csv">
  <dt>Vendor</dt>
  <dd>Mendix</dd>
  <dd><a href="/lowcode/tagged/microsoft">Microsoft</a></dd>
  <dd>Outsystems</dd>
  </dl>


  <dl class="csv">
  <dt>Products</dt>
  <dd><a href="/lowcode/tagged/ms-power-apps">Microsoft Power Apps</a></dd>
  </dl>

  </div>
  <div class="col-6">



  </div>
</div>
