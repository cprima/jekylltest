---
title: thelastweek.in/RPA
permalink: /rpa/
---



<!--<aside>
    <h3>Last week in RPA</h3>
<ul>
{% for post in site.categories.rpa limit:5 %}
{% if post.realm == 'rpa' %}
<li><a class="post-link" href="{{ post.url | prepend: site.baseurl }}">
{{ post.date | date: "%Y week %V" }}
</a>
</li>
{% endif %}
{% endfor %}
</ul>
</aside>//-->


<!-- <div style="text-align: center; padding-top: 0px; padding-bottom: 30px;"><img style="width: 90%; height: auto; max-width: 640px;" src="/biz/marketing/underconstruction.png" /></div>-->

<style>
dl.csv { list-style: none; margin: 0; padding: 0; margin-bottom: 10px;}
dl.csv dd { display: inline; }
dl.csv dd:after { content: ","; }
dl.csv dd:last-child:after { content: ""; }
</style>



<h2 class="post-title">By week</h2>

 <div class="row">
  <div class="col-12">

  <dl class="csv">
  <dt>Recent</dt>
  {% for post in site.categories.rpa limit:5 %}
  {% if post.realm == 'rpa' %}
  <dd><a class="post-link" href="{{ post.url | prepend: site.baseurl }}">
  {{ post.date | date: "%Y-%V" }}
  </a>
  </dd>
  {% endif %}
  {% endfor %}
  </dl>


  </div>
</div>

<hr />

<h2 class="post-title">Directory</h2>

 <div class="row">
  <div class="col-6">

  <dl class="csv">
  <dt>Vendors</dt>
  <dd><a href="/rpa/tagged/automation-anywhere">Automation Anywhere</a></dd>
  <dd><a href="/rpa/tagged/blue-prism">Blue Prism</a></dd>
  <dd><a href="/rpa/tagged/microsoft">Microsoft</a></dd>
  <dd><a href="/rpa/tagged/robocorp">Robocorp</a></dd>
  <dd><a href="/rpa/tagged/uipath">UiPath</a></dd>
  </dl>


  <dl class="csv">
  <dt>Products</dt>
  <dd><a href="/rpa/tagged/ms-power-automate-desktop">Microsoft Power Automate Desktop</a></dd>
  <dd><a href="/rpa/tagged/uipath-orchestrator">UiPath Orchestrator</a></dd>
  <dd><a href="/rpa/tagged/uipath-studio">UiPath Studio</a></dd>
  <dd><a href="/rpa/tagged/uipath-studio-x">UiPath Studio X</a></dd>
  </dl>

  <dl class="csv">
  <dt>Job</dt>
  <dd><a href="/rpa/tagged/job-interview-questions">Job interview questions</a></dd>
  <dd><a href="/rpa/tagged/learning">Learning</a></dd>
  <dd><a href="/rpa/tagged/certification">Certification</a></dd>
  </dl>

  <dl class="csv">
  <dt>Social</dt>
  <dd><a href="/rpa/tagged/community">Community</a></dd>
  <dd><a href="/rpa/tagged/interview">Interviews</a></dd>
  </dl>

  </div>
  <div class="col-6">
  <dl class="csv">
  <dt>Software Engineering</dt>
  <dd><a href="/rpa/tagged/datatypes">datatypes</a></dd>
  <dd><a href="/rpa/tagged/datatable">datatable</a></dd>
  <dd><a href="/rpa/tagged/datetime">datetime</a></dd>
  <dd><a href="/rpa/tagged/regex">regex</a></dd>
  <dd><a href="/rpa/tagged/linq">LINQ</a></dd>
  <dd><a href="/rpa/tagged/vb">VB</a></dd>
  <!-- <dd>logging</dd> -->
  <dd><a href="/rpa/tagged/json">JSON</a></dd>
  <dd><a href="/rpa/tagged/control-flow">control flow</a></dd>
  <dd><a href="/rpa/tagged/errorhandling">error handling</a></dd>
  <dd><a href="/rpa/tagged/exceptions">exceptions</a></dd>
  <dd><a href="/rpa/tagged/debugging">debugging</a></dd>
  <dd><a href="/rpa/tagged/logging">logging</a></dd>
  <dd><a href="/rpa/tagged/selectors">selectors</a></dd>
  </dl>

  <dl class="csv">
  <dt>Automated Applications</dt>
  <dd><a href="/rpa/tagged/excel">Excel</a></dd>
  <dd><a href="/rpa/tagged/browser">Browser</a></dd>
  <dd><a href="/rpa/tagged/scraping">Scraping</a></dd>
  <dd><a href="/rpa/tagged/email-automation">Mail Automation</a></dd>
  <dd><a href="/rpa/tagged/apis">APIs</a></dd>
  <dd><a href="/rpa/tagged/sap">SAP</a></dd>
  </dl>

  <dl class="csv">
  <dt>Methodology</dt>
  <dd><a href="/rpa/tagged/business-value">Business Value</a></dd>
  <dd><a href="/rpa/tagged/citizen-development">Citizen Development</a></dd>
  <!--<dd>Effort Estimation</dd>-->
  <dd><a href="/rpa/tagged/solution-design">Solution Design</a></dd>
  <dd><a href="/rpa/tagged/maintainability">Maintainability</a></dd>
  <dd><a href="/rpa/tagged/code-review">Code Review</a></dd>
  <dd><a href="/rpa/tagged/cicd">CI/CD</a></dd>
  <dd><a href="/rpa/tagged/testing">Testing</a></dd>
  </dl>


  </div>
</div>
